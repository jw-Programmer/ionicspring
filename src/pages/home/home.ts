import { AuthService } from './../../services/auth.service';
import { CredenciaisDto } from './../../models/credenciais.dto';
import { Component } from '@angular/core';
import { NavController, IonicPage, MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  cred: CredenciaisDto = {
    email: "",
    senha: ""
  }

  constructor(public navCtrl: NavController,
    public menu: MenuController,
    public auth: AuthService) {

  }
  ionViewWillEnter() {
    this.menu.swipeEnable(false)
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true)
  }

  ionViewDidEnter() {
    this.auth.refreshToken().subscribe(response => {
      this.auth.sucessfulLogin(response.headers.get("Authorization"))
      this.navCtrl.setRoot("CategoriasPage")
    },
      error => { })
    console.log(this.cred)
  }


  login() {
    this.auth.authenticate(this.cred).subscribe(response => {
      this.auth.sucessfulLogin(response.headers.get("Authorization"))
    },
      error => { })
    console.log(this.cred)
    this.navCtrl.setRoot("CategoriasPage")
  }

  signUp() {
    this.navCtrl.push("SignupPage")
  }

}
