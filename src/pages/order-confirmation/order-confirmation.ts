import { PedidoService } from './../../services/domain/pedido.service';
import { ClienteService } from './../../services/domain/cliente.service';
import { EnderecoDto } from './../../models/endereco.dto';
import { ClienteDto } from './../../models/cliente.dto';
import { CartService } from './../../services/domain/cart.service';
import { CartItem } from './../../models/cart_item';
import { PedidoDto } from './../../models/pedido.dto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the OrderConfirmationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-confirmation',
  templateUrl: 'order-confirmation.html',
})
export class OrderConfirmationPage {

  pedido: PedidoDto
  cartItems: CartItem[];
  cliente: ClienteDto;
  endereco: EnderecoDto;
  codPedido: string

  constructor(public navCtrl: NavController, public navParams: NavParams, public cartService: CartService, public clienteservice: ClienteService, public pedidoService: PedidoService) {
    this.pedido = this.navParams.get("pedido")
  }

  ionViewDidLoad() {
    this.cartItems = this.cartService.getCart().items
    this.clienteservice.findById(this.pedido.cliente.id).subscribe(response => {
      this.cliente = response as ClienteDto
      this.endereco = this.findEndereco(this.pedido.enderecoDeEntrega.id, response["enderecos"])
    },
      error => {
        this.navCtrl.setRoot("HomePage")
      })
  }

  private findEndereco(id: string, list: EnderecoDto[]): EnderecoDto {
    let position = list.findIndex(x => x.id == id)
    return list[position]
  }

  checkout() {
    this.pedidoService.insert(this.pedido).subscribe(response => {
      this.cartService.createOrClearCart()
      this.codPedido = this.extractId(response.headers.get("location"))
    },
      error => {
        if (error.status == 403) {
          this.navCtrl.setRoot("HomePage")
        }
      })
  }

  total() {
    return this.cartService.total()
  }

  back() {
    this.navCtrl.setRoot('CartPage')
  }

  home(){
    this.navCtrl.setRoot("CategoriasPage")
  }

  private extractId(location: string): string {
    let position = location.lastIndexOf('/')
    return location.substring(position + 1, location.length)
  }

}
