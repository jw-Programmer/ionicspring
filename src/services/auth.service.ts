import { CartService } from './domain/cart.service';
import { StorageService } from './storage.service';
import { API_CONFIG } from './../config/api.config';
import { HttpClient } from '@angular/common/http';
import { CredenciaisDto } from './../models/credenciais.dto';
import { Injectable } from '@angular/core';
import { LocalUser } from '../models/local_user';
import { JwtHelper } from 'angular2-jwt'

@Injectable()
export class AuthService {

    jwtHelper: JwtHelper = new JwtHelper()

    constructor(public http: HttpClient, public store: StorageService, public cartService: CartService) {

    }

    authenticate(credenciais: CredenciaisDto) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/login`, credenciais,
            {
                observe: "response",
                responseType: "text"
            })
    }

    sucessfulLogin(authorizationValue: string) {
        let tok = authorizationValue.substring(7)
        let user: LocalUser = {
            token: tok,
            email: this.jwtHelper.decodeToken(tok).sub
        }
        this.store.setLocalUser(user)
        this.cartService.createOrClearCart()
    }

    logout() {
        this.store.setLocalUser(null)
    }

    refreshToken() {
        return this.http.post(
            `${API_CONFIG.baseUrl}/auth/refresh_token`, {},
            {
                observe: "response",
                responseType: "text"
            })
    }
}