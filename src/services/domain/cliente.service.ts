import { ImageUtilService } from './../image-util.service';
import { StorageService } from './../storage.service';
import { API_CONFIG } from './../../config/api.config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Rx';
import { ClienteDto } from '../../models/cliente.dto';

@Injectable()
export class ClienteService {
    constructor(public http: HttpClient, public store: StorageService, public imageService: ImageUtilService) { }

    findById(id: string) {
        return this.http.get(`${API_CONFIG.baseUrl}/clientes/${id}`);
    }

    findByEmail(email: string) {
        return this.http.get(
            `${API_CONFIG.baseUrl}/clientes/email?email=${email}`)
    }

    getImageFromBucket(id: string): Observable<any> {
        let url = `${API_CONFIG.bucketBaseUrl}cp${id}.jpg`
        return this.http.get(url, { responseType: "blob" });
    }

    insert(obj: ClienteDto) {
        return this.http.post(`${API_CONFIG.baseUrl}/clientes`,
            obj,
            {
                observe: "response",
                responseType: "text"
            })
    }

    uploudPicture(picture) {
        let pictureBlob = this.imageService.dataUriToBlob(picture)
        let formDate: FormData = new FormData();
        formDate.set("file", pictureBlob, 'file.png')
        return this.http.post(`${API_CONFIG.baseUrl}/clientes/picture`,
            formDate,
            {
                observe: "response",
                responseType: "text"
            })
    }
}