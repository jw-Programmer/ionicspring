import { Observable } from 'rxjs/Rx';
import { API_CONFIG } from './../../config/api.config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProdutoDto } from '../../models/produto.dto';

@Injectable()
export class ProdutoService {
    constructor(private http: HttpClient) { }

    findById(id_categoria) {
        return this.http.get<ProdutoDto>(`${API_CONFIG.baseUrl}/produtos/${id_categoria}`)
    }

    findByCategoria(id_categoria: string, page: number, linesPerPage: number) {
        return this.http.get(`${API_CONFIG.baseUrl}/produtos/?categorias=${id_categoria}&page=${page}&linesPerPage=${linesPerPage}`)
    }

    getSmallImageFromBucket(id: string): Observable<any> {
        let url = `${API_CONFIG.bucketBaseUrl}prod${id}-small.jpg`
        return this.http.get(url, { responseType: "blob" })
    }

    getImageFromBucket(id: string): Observable<any> {
        let url = `${API_CONFIG.bucketBaseUrl}prod${id}.jpg`
        return this.http.get(url, { responseType: "blob" })
    }


}